public class Main {
    public static void main(String[] args) {
        GeometryCalculator calculator = new GeometryCalculator();

        System.out.println("Площадь круга: " + calculator.getCircleSquare(34));
        System.out.println("Объём сферы: " + calculator.getSphereVolume(4));
        System.out.println("Площадь  треугольника: " + calculator.getTriangleSquare(30, 30, 40));
    }


}
