public class GeometryCalculator {
    public double getCircleSquare(double radiusCircle) // прощадь круга
    {
        double s = Math.PI * Math.pow(radiusCircle, 2);
        return s;
    }

    public double getSphereVolume(double radiusVolume) // объём сферы
    {
        double volume = 4 / 3 * Math.PI * Math.pow(radiusVolume, 3);
        return volume;
    }

    public static boolean isTrianglePosible(double a, double b, double c) {
        return a < b + c && b < a + c && c < a + b;
    }

    public double getTriangleSquare(double a, double b, double c) // прощадь  треугольника
    {
        if (isTrianglePosible(a, b, c)) {
            System.out.println("Можно построить треугольник");
            double p = (a + b + c) / 2;
            double p1 = (p - a);
            double p2 = (p - b);
            double p3 = (p - c);
            double pp = p * p1 * p2 * p3;
            double s = Math.sqrt(pp);
            return s;
        }
        return -1.0;

    }
